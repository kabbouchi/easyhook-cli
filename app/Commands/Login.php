<?php

namespace App\Commands;

use Zttp\Zttp;
use Zttp\ZttpResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class Login extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'login';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'login with your easyhook.io credentials.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (! file_exists($file = database_path('database.sqlite'))) {
            touch($file);
        }

        $this->callSilent('migrate', ['--force' => true]);

        $this->info("Enter your easyhook.io credentials.");

        if ($email = $this->ask('Email')) {
            $password = $this->secret("Password");
            $name = 'easyhook-cli';

            $this->info('Login...');

            /** @var ZttpResponse $response */
            $response = Zttp::post('https://easyhook.io/api/generate-access-tokens', compact('email', 'password', 'name'));

            if ($response->isOk()) {
                $this->info('Logged in successfully.');

                $token = $response->body();
                DB::table('tokens')->insert(compact('name', 'token'));

                $this->info('Token has been added successfully.');
            } else {
                $this->error('These credentials do not match our records.');
            }
        };
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
