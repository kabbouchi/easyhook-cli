<?php

namespace App\Commands;

use Zttp\Zttp;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class Deploy extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'deploy {webhook? : The webhook Id} ';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Deploy website';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (! file_exists($file = database_path('database.sqlite'))) {
            touch($file);
        }

        $this->callSilent('migrate', ['--force' => true]);

        $webhook = $this->argument('webhook');

        $token = DB::table('tokens')->value('token');

        if ($webhook) {
            /** @var ZttpResponse $response */
            $response = Zttp::withHeaders(['Authorization' => "Bearer {$token}"])->post("https://easyhook.io/api/v1/hooks/{$webhook}/deploy");
            if ($response->isOk()) {
                $this->info('Queued for deployment!');
            }
        } else {
            /** @var ZttpResponse $response */
            $response = Zttp::withHeaders(['Authorization' => "Bearer {$token}"])->get("https://easyhook.io/api/v1/hooks");
            if ($response->isOk()) {
                $hooks = collect($response->json());

                $this->info('Usage: easyhook deploy {ID}');

                $this->table(['ID','Name'], $hooks->map(function ($hook) {
                    return [$hook['id'], $hook['name']];
                }));
            }
        }
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
